# Lange Nacht der Wissenschaften 2017

The goal of this project is to simulate an electron gun and a multi-slit experiment. 

## Instructions:

1. Clone this repository

    `git clone git@bitbucket.org:janbehrends/lange-nacht-2017.git`

2. Go to the folder lange-nacht/

    `cd lange-nacht/`

3. Start a webserver

    `python -m SimpleHTTPServer`
    
    or, with python3

    `python3 -m http.server`

    The output will look like

    `Serving HTTP on 0.0.0.0 port 8000 ...`

4. Go to the website

    `http://0.0.0.0:8000`

    or a similar one, depending on the previous output.

## To do

- M screens with N drilled holes
- add sound
- better colors
- automatically adjust number of slits
- dropdown for number of slits?
