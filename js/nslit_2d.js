var div = 60;
var xlen = 1;

var canvas = document.getElementById("myCanvas");
canvas.style.background = "#D0D0D0";

var slit_xposition = Math.floor(canvas.width/3);
var screen_xposition = Math.floor(canvas.width/1.8);
var ysize = Math.floor(0.5*canvas.height);
var xsize = 200;

// -- Define colorscheme --
// Buttons
var button_color = '#002F70';
var button_color_hover = '#0046A8';
var button_color_pressed = '#1F5C57';
var button_color_text = '#DDDDED';
// Screen
var screen_color = 'white';
var screen_color_hover = '#F0F0F0';
// Electron count
var counter_color = '#5C0000';
var counter_color_hover = '#874E4A';
// Electrons
var electron_color = '#C05020';
var electron_color_marker = 'black';

// -- Describe items by hovering with mouse --
var description = new PointText(new Point(100,ysize*3/4));
description.justification = 'left';
description.fillColor = 'black';
description.fontSize = '16pt';
description.fontWeight = 'bold';


var slit_width = 5;
// width of the slit in units of the wavelength
var slit_distance = 10;
// distance beween the slits in units of the wavelength
var slit_N = 10;
// number of slits

var normalization = [1,0.562665,1.12433,1.68573,2.24703,2.80826,3.36946,3.93062,4.49176,5.05289,5.61401,6.17511,6.73621,7.2973,7.85839,8.41947,8.98054,9.54161,10.1027,10.6637,11.2248,11.7859,12.3469,12.908,13.469,14.0301,14.5911,15.1522,15.7132,16.2743,16.8353,17.3964,17.9574,18.5184,19.0795,19.6405,20.2016,20.7626,21.3236,21.8847,22.4457,23.0067,23.5678,24.1288,24.6898,25.2509,25.8119,26.3729,26.934,27.495,28.056,28.617,29.1781,29.7391,30.3001,30.8612,31.4222,31.9832,32.5442,33.1053,33.6663,34.2273,34.7884,35.3494,35.9104,36.4714,37.0325,37.5935,38.1545,38.7155,39.2766,39.8376,40.3986,40.9596,41.5206,42.0817,42.6427,43.2037,43.7647,44.3258,44.8868,45.4478,46.0088,46.5699,47.1309,47.6919,48.2529,48.8139,49.375,49.936,50.497,51.058,51.619,52.1801,52.7411,53.3036,53.8631,54.4241,54.9852,55.5462,56.1072];
// The probability distribution is not normalized; these are values obtained
// with Mathematica, integrated from -L/2 to L/2 for these particular values of
// slit_width=5 and slit_distance = 10. Note that this normalization has to
// change whenever slit_width or slit_distance are changed

function sinc(x) {
    // Need to be defined for proper probability distribution
    if (x===0) {
        result = 1;
    } else {
        result = Math.sin(x)/(x);
    }
    return result;
}
function fraunhofer(x) {
    arg = Math.PI*slit_distance*x;
    result = sinc(slit_width*x)*Math.sin(slit_N*arg)/Math.sin(arg);
    return Math.pow(result,2)/normalization[slit_N];
}
function gaussian(x) {
    arg = Math.pow(x,2)/2;
    norm = Math.sqrt(2*Math.PI);
    return Math.exp(-arg)/norm;
}

function create_setup() {
    // Creates whole setup
    
    // electron Gun
    var x00 = new Point(100,ysize/2-20);
    var x01 = new Point(200,ysize/2-40);
    var x10 = new Point(100,ysize/2+20);
    var x11 = new Point(200,ysize/2+40);
    var x_arc = new Point(230,ysize/2);

    var path = Path.Line(x00,x01);
    path.add(x11);
    path.add(x10);
    path.add(x00);

    var electronGun = new CompoundPath({
        children: [
            path,
            new Path.Arc({
                from: x01,
                through: x_arc,
                to: x11
            }),
        ],
        fillColor: 'black'
    });
    electronGun.onMouseEnter = function(event) {
        this.fillColor = '#505050';
        description.content='Elektronenkanone';
    };
    electronGun.onMouseLeave = function(event) {
        this.fillColor = 'black';
        description.content=' ';
    };

    var spiral = new Path({
        strokeColor: 'orange',
        strokeWidth: 3
    });
    var len = 8;
    for (var i=0; i<len; i++) {
        spiral.add( (x00+x10)/2 + (x10-x00)*(2*(i%2)-1)/4 + (x11+x01-x00-x10)*(i+1)/(4*len) );
    }
    spiral.smooth();

    spiral.onMouseEnter = function(event) {
        this.strokeColor = 'yellow';
        description.content='Glühkathode';
    };
    spiral.onMouseLeave = function(event) {
        this.strokeColor = 'orange';
        description.content=' ';
    };
    // Single Electron
    var single_electron_group = new Group();
    var single_electron_button = new Path.Rectangle({
        center: [70,30],
        size: [100,30],
        fillColor: button_color
    });
    var single_electron_text = new PointText(new Point(70,33));
    single_electron_text.content = 'Ein Elektron';
    single_electron_text.justification = 'center';
    single_electron_text.fillColor = button_color_text;
    
    single_electron_group.addChild(electronGun);
    single_electron_group.addChild(single_electron_button);
    single_electron_group.addChild(single_electron_text);
    single_electron_group.addChild(spiral);

    single_electron_group.onMouseDown = function(event) {
        add_electron();
        spiral.strokeColor = 'yellow';
        single_electron_button.fillColor = button_color_pressed;
    };
    single_electron_group.onMouseUp = function(event) {
        spiral.strokeColor = 'orange';
        single_electron_button.fillColor = button_color;
        electron_collection.fillColor = electron_color_marker;
        //electron_collection.removeChildren();
    };

    // 10 Electrons
    var ten_electrons_group = new Group();

    var ten_electrons_button = new Path.Rectangle({
        center: [70,80],
        size: [100,30],
        fillColor: button_color
    });
    var ten_electrons_text = new PointText(new Point(70,83));
    ten_electrons_text.content = '10 Elektronen';
    ten_electrons_text.justification = 'center';
    ten_electrons_text.fillColor = button_color_text;

    ten_electrons_group.addChild(ten_electrons_button);
    ten_electrons_group.addChild(ten_electrons_text);
    ten_electrons_group.onMouseDown = function(event) {
        ten_electrons_button.fillColor = button_color_pressed;
        for (var i=0; i<10; i++) {
            add_electron();
        }
        spiral.strokeColor = 'yellow';
    };
    ten_electrons_group.onMouseUp = function(event) {
        ten_electrons_button.fillColor = button_color;
        spiral.strokeColor = 'orange';
        electron_collection.fillColor = electron_color_marker;
        //electron_collection.removeChildren();
    };
    // 100 Electrons
    var hundred_electrons_group = new Group();

    var hundred_electrons_button = new Path.Rectangle({
        center: [70,130],
        size: [100,30],
        fillColor: button_color
    });
    var hundred_electrons_text = new PointText(new Point(70,133));
    hundred_electrons_text.content = '100 Elektronen';
    hundred_electrons_text.justification = 'center';
    hundred_electrons_text.fillColor = button_color_text;

    hundred_electrons_group.addChild(hundred_electrons_button);
    hundred_electrons_group.addChild(hundred_electrons_text);
    hundred_electrons_group.onMouseDown = function(event) {
        hundred_electrons_button.fillColor = button_color_pressed;
        for (var i=0; i<100; i++) {
            add_electron();
        }
        spiral.strokeColor = 'yellow';
    };
    hundred_electrons_group.onMouseUp = function(event) {
        hundred_electrons_button.fillColor = button_color;
        spiral.strokeColor = 'orange';
        electron_collection.fillColor = electron_color_marker;
        //electron_collection.removeChildren();
    };
    // Reset Button
    var resetGroup = new Group();
    var resetButton = new Path.Rectangle({
        center: [70,180],
        size: [100,30],
        fillColor: button_color
    });
    var resetText = new PointText(new Point(70,183));
    resetText.content = 'Zurücksetzen';
    resetText.justification = 'center';
    resetText.fillColor = button_color_text;
    resetGroup.addChild(resetButton);
    resetGroup.addChild(resetText);

    resetGroup.onMouseDown = function(event) {
        resetButton.fillColor = button_color_pressed;
        electron_collection.removeChildren();
        counter.removeChildren();
    };
    resetGroup.onMouseUp = function(event) {
        resetButton.fillColor = button_color;
        for (var i=0; i<div; i++) {
            counting[i] =0;
        }
    };
    // Screen with slit_N slits
    var slits = new Group();
    var pos = new Point(slit_xposition,(ysize/div)*0.5);
    var size = new Size(10,0.47*ysize);
    slits.addChild(new Path.Rectangle(pos,size));
    pos = new Point(slit_xposition,0.53*ysize + (ysize/div)*0.5);
    slits.addChild(new Path.Rectangle(pos,size));

    var spacing = 0.06*ysize/(2*slit_N - 1);
    for (i=0; i<slit_N; i++) {
        //Add slits
        var tmp_pos = new Point(slit_xposition,(ysize/div)*0.5 + 0.47*ysize+spacing*(2*i+1));
        var tmp_size= new Size(10,spacing);
        slits.addChild(new Path.Rectangle(tmp_pos,tmp_size));
        console.log(i);
    }
    slits.fillColor = 'black';

    slits.onMouseEnter = function(event) {
        this.fillColor = '#505050';
        description.content='Blende mit '+slit_N.toString()+' Spalten';
    };
    slits.onMouseLeave = function(event) {
        this.fillColor = 'black';
        description.content=' ';
    };
    // Screen
    for (i=0; i<div; i++) {
        counting[i] = 0;
    }
    point = new Point(screen_xposition,(ysize/div)*0.5);
    size = new Size(xsize,ysize);
    var screen = new Path.Rectangle(point,size);
    screen.fillColor = screen_color;

    screen.onMouseEnter = function(event) {
        this.fillColor = screen_color_hover;
        description.content='Beobachtungsschirm';
    };
    screen.onMouseLeave = function(event) {
        this.fillColor = screen_color;
        description.content=' ';
    };
}

// Count all electrons
var counting = [];
// Global counter
var counter = new Group();
// Create setup
create_setup();
// Global variable electron collection
var electron_collection = new Group();

counter.onMouseEnter = function(event) {
    this.fillColor = counter_color_hover;
    description.content='Anzahl der Elektronen';
};
counter.onMouseLeave = function(event) {
    this.fillColor = counter_color;
    description.content=' ';
};

function random_gaussian(mu,sigma) {
    // Generates random Gaussian distributed value via
    // Box-Muller tansform
    // https://en.wikipedia.org/wiki/Box%E2%80%93Muller_transform
    var U = Math.random();
    var V = Math.random();
    var X = Math.sqrt(-2*Math.log(U))*Math.cos(2*Math.PI*V);
    var Y = Math.sqrt(-2*Math.log(U))*Math.sin(2*Math.PI*V);
    return mu + sigma*X;
}


function add_electron() {
    var random;
    var M = 1;
    // Create random number drawn from normal distribution between -.5 and .5
    var r = random_gaussian(0,1/4);
    while (Math.abs(r)>0.5) {
        r = random_gaussian(0,1/4);
    }
    // Another random number between 0 and 1
    var u = Math.random();

    // 0 to 1
    while (u>fraunhofer(r)/(M*gaussian(r))) {
        r = random_gaussian(0,1/4);
        while (Math.abs(r)>0.5) {
            r = random_gaussian(0,1/4);
        }
        u = Math.random();
    }
    var v  = screen_xposition + xsize*Math.random();
    random = ysize/2 + (ysize/div)*0.5 + r*ysize;

    var circle = new Path.Circle({
        center: [v,random],
        radius: 2,
        fillColor: electron_color
    });
    electron_collection.addChild(circle);
    
    var c = Math.floor(random/(ysize/div));
    counting[c]+= xlen;
    var point = new Point(screen_xposition+xsize+counting[c],(ysize/div)*(c+0.5));
    var size = new Size(xlen,ysize/div);
    var rect = new Path.Rectangle(point,size);
    rect.fillColor=counter_color;

    counter.addChild(rect);
}
